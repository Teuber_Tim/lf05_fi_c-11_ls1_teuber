import java.util.Scanner;

public class MittelwertMitMethoden {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double zahl1, zahl2,mittelwert;
		Scanner myScanner = new Scanner(System.in);
		
		zahl1 = eingabe(myScanner,"Bitte eben sie die erste Zahl ein: ");
		zahl2 = eingabe(myScanner,"Bitte geben sie die zweite Zahl ein: ");
		mittelwert = mittelwertBerechnung(zahl1,zahl2);
		ausgabe ("Der Mittelwert von %.2f und %.2f ist %.2f!", zahl1,zahl2,mittelwert);
		myScanner.close();

	}

	public static double eingabe(Scanner myScanner, String text) {
		
		System.out.println(text);
		double eingabe = myScanner.nextDouble();
		return eingabe;
	}
	public static double mittelwertBerechnung(double x, double y) {
		double ergebnis = (x + y)/2;
		return ergebnis;
	}
	public static void ausgabe (String text, double x, double y, double z) {
		System.out.printf(text,x,y,z);
	}
}
