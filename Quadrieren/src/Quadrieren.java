public class Quadrieren {

	public static void main(String[] args) {
		double x = 5;
		titel();
		
		double ergebnis = rechnung(x);
		ausgabe(x, ergebnis);
		

	}

	// (E) "Eingabe"
	// Wert f�r x festlegen:
	// ===========================
	public static void titel() {
		System.out.println("Dieses Programm berechnet die Quadratzahl x�");
		System.out.println("---------------------------------------------");
	}

	public static double rechnung(double x) {

		// (V) Verarbeitung
		// Mittelwert von x und y berechnen:
		// ================================
		double ergebnis = x * x;
		return ergebnis;
	}

	// (A) Ausgabe
	// Ergebnis auf der Konsole ausgeben:
	// =================================
	public static void ausgabe(double x, double ergebnis) {
		System.out.printf("x = %.2f und x�= %.2f\n", x, ergebnis);
	}

}
