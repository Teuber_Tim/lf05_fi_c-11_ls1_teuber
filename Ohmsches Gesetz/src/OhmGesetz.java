import java.util.Scanner;

public class OhmGesetz {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double ohm, volt, ampere;
		System.out.printf("Willkommen! \nDieses Programm berechnet den elektrischen Widerstand, die Spannung oder die Stromst�rke mit Hilfe des Ohmschen Gesetzes.");
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Bitte w�hlen sie eine der folgenden Gr��en mittels des Formelzeichens aus: Widerstand [R], Spannung [U] oder Stromst�rke [I] ");
		char fz = tastatur.next().charAt(0);
		switch (fz) {
		case 'R': {
			volt = eingabeDouble(tastatur, "Bitte geben sie die vorliegende Spannung in Volt ein: ");
			ampere = eingabeDouble(tastatur, "Bitte geben sie die vorliegende Stromst�rke in Ampere ein: ");
			ohm = volt / ampere;
			System.out.printf("Der elektrische Wiederstand betr�gt %.2f Ohm!", ohm);
			break;
		}
		case 'U': {
			ohm = eingabeDouble(tastatur, "Bitte geben sie den vorliegenden elektrischen Widerstand in Ohm ein: ");
			ampere = eingabeDouble(tastatur, "Bitte geben sie die vorliegende Stromst�rke in Ampere ein: ");
			volt = ohm*ampere;
			System.out.printf("Die Spannung betr�gt %.2f Volt!", volt);
			break;
		}
		case 'I': {
			volt = eingabeDouble(tastatur, "Bitte geben sie die vorliegende Spannung in Volt ein: ");
			ohm = eingabeDouble(tastatur, "Bitte geben sie den vorliegenden elektrischen Widerstand in Ohm ein: ");
			ampere = volt / ohm;
			System.out.printf("Die Stromst�rke betr�gt %.2f Ampere!", ampere);
			break;
			
		}
		default:
			System.out.println("Falsche Eingabe! Bitte Programm neu starten!");
		}
		
		tastatur.close();
	}

	public static double eingabeDouble(Scanner tastatur, String text) {
		System.out.print(text);
		double wert = tastatur.nextDouble();
		return wert;
	}
}
