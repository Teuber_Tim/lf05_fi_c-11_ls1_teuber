﻿import java.util.Locale;
import java.util.Scanner;

class Fahrkartenautomat {
	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);

		float zuZahlenderBetrag; // beinhaltet den zu zahlenden Betrag für die Tickets, Neudefinition nach
									// multiplikation mit anzahlTickets
		float eingezahlterGesamtbetrag = 0; // beinhaltet die Summe aller eingeworfenen Münzen (zuZahlenderBetrag)
//		float eingeworfeneMünze; // beinhaltet die Eingabe des Nutzers bezüglcih des Münzeinwurfs, Summe der
		// Münzeiwürfe definieren eingezahlterGesamtbetrag
//		float rückgabebetrag; // wird zum Abgleich des Rückgeldes benötigt, beinhaltet die Differenz von
		// zuZahlenderBetrag und eingezahlterGesamtbetrag
//		byte anzahlTickets; // anzahlTickets, vom Type Byte, da selten mehr als 128 Tickets benötigt werden.
		// Multiplikation mit zuZahlenderBetrag
		boolean an = true;
		while (an) {
			zuZahlenderBetrag = fahrkartenbestellungErfassen(tastatur, "Zu zahlender Betrag (EURO): ",
					"Anzahl der Tickets:");
			eingezahlterGesamtbetrag = fahrkartenBezahlen(tastatur,
					"Noch zu zahlen:  %.2f EURO. %n Bitte Geld einwerfen:", zuZahlenderBetrag);
			fahrkartenAusgeben();
			rückgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
		}
		tastatur.close();
	}

	// Fahrscheinausgabe
	// -----------------
	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				warte(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");
	}

	public static void warte(int millisekunden) throws InterruptedException {
		Thread.sleep(millisekunden);
	}

	// Rückgeldberechnung und -Ausgabe
	// -------------------------------
	public static void muenzAusgabe(String text1, String text2) {
		System.out.println(text1 + " " + text2);
	}

	public static void rückgeldAusgeben(float eingezahlterGesamtbetrag, float zuZahlenderBetrag) {

		float rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		System.out.printf("Der Rückgabebetrag in Höhe von  %.2f EURO %n", rückgabebetrag);
		System.out.println("wird in folgenden Münzen ausgezahlt:");
		if (rückgabebetrag > 0.00f) {

			while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
			{
				muenzAusgabe("2", "EURO");
				int temp = Math.round((rückgabebetrag -= 2.0) * 100);
				rückgabebetrag = (float) temp / 100;

			}
			while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
			{
				muenzAusgabe("1", "EURO");
				int temp = Math.round((rückgabebetrag -= 1.0) * 100);
				rückgabebetrag = (float) temp / 100;
			}
			while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
			{
				muenzAusgabe("50", "CENT");
				int temp = Math.round((rückgabebetrag -= 0.5) * 100);
				rückgabebetrag = (float) temp / 100;
			}
			while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
			{
				muenzAusgabe("20", "CENT");
				int temp = Math.round((rückgabebetrag -= 0.2) * 100);
				rückgabebetrag = (float) temp / 100;
			}
			while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
			{
				muenzAusgabe("10", "CENT");
				int temp = Math.round((rückgabebetrag -= 0.1) * 100);
				rückgabebetrag = (float) temp / 100;
			}
			while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
			{
				muenzAusgabe("5", "CENT");
				int temp = Math.round((rückgabebetrag -= 0.05) * 100);
				rückgabebetrag = (float) temp / 100;
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wünschen Ihnen eine gute Fahrt.");
	}

	public static float fahrkartenbestellungErfassen(Scanner tastatur, String text, String text2) {
		boolean fehler = false;
		byte auswahl = 0;
		float preis = 0, zuZahlenderBetrag = 0;
		do {
			do {
				fehler = false;
				System.out.printf("\n\nWillkommen!\n" + "Bitte wählen sie ein der folgenden Fahrkartentariefe:\n"
						+ "Einzelfahrausweis Regeltarif AB [2,90 EUR] (1)\n"
						+ "Tageskarte Regltarif AB [8,60 EUR] (2)\n"
						+ "Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)\n" + "Bezahlen (9)\n");
				auswahl = tastatur.nextByte();
				if (auswahl < 1 || (auswahl > 3 && auswahl != 9)) {
					System.out.println("Fehlerhafte Eingabe");
					fehler = true;
				}
			} while (fehler != false);
			switch (auswahl) {
			case 1:
				preis = 2.90f;
				break;
			case 2:
				preis = 8.60f;
				break;
			case 3:
				preis = 23.50f;
				break;
			case 9:
				break;
			}
			do {
				if (auswahl==9) {
					break;
				}
				fehler = false;
				System.out.println(text2);
				byte anzahl = tastatur.nextByte();
				if (anzahl > 10 || anzahl <= 0) {
					System.out.println("Es können maximal 10 Fahrkarten auf einmal gekauft werden!");
					anzahl = 1;
					fehler = true;
				}
				zuZahlenderBetrag+=preis * anzahl; // zuZahlenderBetrag wird mit anzahlTickets multipliziert und neu
													// definiert.
				System.out.printf("Zwischensumme: %.2f EUR", zuZahlenderBetrag);

			} while (fehler != false);
		} while (auswahl != 9);
		return zuZahlenderBetrag;
	}

	public static float fahrkartenBezahlen(Scanner tastatur, String text, float zuZahlenderBetrag) {
		float eingeworfeneMünze;
		float eingezahlterGesamtbetrag = 0.00f;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.printf(Locale.US, text, zuZahlenderBetrag - eingezahlterGesamtbetrag);
			eingeworfeneMünze = tastatur.nextFloat();
			eingezahlterGesamtbetrag += eingeworfeneMünze;
		}
		return eingezahlterGesamtbetrag;
	}

}