//AUfgabe 4- Arbeitsblatt Array-�bungen mit Methoden
import java.util.Scanner;
public class ArraysMitMethoden {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner tastatur = new Scanner(System.in);
		System.out.printf("Willkommen\nDieses Programm dient zur Ausgabe einer Temperaturtabelle in Fahrenheit und Celsius!");

		System.out.printf("\nBiite geben sie die gew�nschte L�nge der Tabelle an: ");
		int laenge = tastatur.nextInt();
		System.out.printf("Bitte geben sie die Schrittweite ein: ");
		double schrittweite = tastatur.nextDouble();
		
		System.out.printf("\n  Temperaturtabelle  "+"\n=====================\n");
		temperaturTabelle(laenge, schrittweite);
		tastatur.close();
		
	}
	public static void temperaturTabelle(int laenge, double schrittweite) {
		
		double fahrenheit = 0.0;
		for (int i=0; i<laenge; i++) {
		double celsius = (5.0/9.0)*(fahrenheit-32.0);
		String[] einheit = {"�F","�C"};
		
		double[] temperatur = {fahrenheit,celsius};
		
		System.out.printf("\n"+"%.2f"+einheit[0]+"   "+ "%.2f"+einheit[1],temperatur[0],temperatur[1]);
		fahrenheit +=schrittweite;
		}
		
		
	}
}
