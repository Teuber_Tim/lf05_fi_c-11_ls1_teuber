import java.util.Scanner;

public class Rom {

	public static void main(String[] args) {
		String rZahl;int dZahl=0,l=0,n=0,m=0,o= 0; char rZeichen = 0,rZeichenNext=0,rZeichenAfterNext=0,rZeichen2AfterNext=0,rZeichenBefore=0;
		Scanner tastatur = new Scanner(System.in);
		System.out.printf("Willkommen! \nDieses Programm errechnet R�miche Zahlen in Dezimalzahlen um.\n");
		rZahl = eingabeString(tastatur, "Bitte geben sie eine R�mische Zahl ein: ");
		char[] rZiffer= rZahl.toCharArray();
		
		for(int i=0; i < rZiffer.length; i++ ) {
			rZeichen = rZiffer[i];
			if (rZeichen != 'I' && rZeichen != 'V' && rZeichen != 'X' && rZeichen != 'L' && rZeichen != 'C' && rZeichen!='D'&& rZeichen!='M') {
				System.out.println("Fehlerhafte Eingabe");
				break;
			}
			l=i-1;
			n=i+1;
			m=i+2;
			o=i+3;
			if (l < 0) {
				l++;
			}
			if (n==rZiffer.length) {
				n=rZiffer.length;
				n--;
				m--;m--;
				o--;o--;o--;
			}
			if (m==rZiffer.length) {
				m=rZiffer.length;
				m--;
				o--;o--;
			}
			if (o==rZiffer.length) {
				o=rZiffer.length;
				o--;
			}
			rZeichenBefore= rZiffer[l];
			rZeichenNext= rZiffer[n];
			rZeichenAfterNext = rZiffer[m];
			rZeichen2AfterNext = rZiffer[o];
			
			if((rZeichen =='I' && rZeichenBefore=='I') && (rZeichenNext=='V'|| rZeichenNext=='X')){
				System.out.println("Es D�rfen nur 2 kleiner Zeichen vor einem Gr��eren stehen!");
				break;
			}
			else if((rZeichen =='X' && rZeichenBefore=='X') && (rZeichenNext=='L'||rZeichenNext=='C')){
				System.out.println("Es D�rfen nur 2 kleiner Zeichen vor einem Gr��eren stehen!");
				break;
			}
			else if((rZeichen =='C' && rZeichenBefore=='C') && (rZeichenNext=='D'|| rZeichenNext=='M')){
				System.out.println("Es D�rfen nur 2 kleiner Zeichen vor einem Gr��eren stehen!");
				break;
			}
			else if (rZeichen == rZeichenNext && rZeichen == rZeichenAfterNext && rZeichen == rZeichen2AfterNext) {
				System.out.println("Es d�rfen maximal 3 gleiche Zeichen aufeinander folgen.");
				break;
			}
			else {
				dZahl=sumUpRomToDec(rZeichen, rZeichenNext, dZahl);
				//dZahl += zahl;
			}}
		if ((rZiffer.length >1 && dZahl > 1)||(rZiffer.length == 1 && dZahl == 1)){
			System.out.printf("Die r�misch Zahl %s entpricht der Dezimalzahl %d.",rZahl,dZahl);
		}
					
		tastatur.close();
	}

	public static String eingabeString(Scanner tastatur, String text) {
		System.out.println(text);
		String rZahl = tastatur.next();
		return rZahl;
	}

	public static int sumUpRomToDec(char rZiffer, char rZifferNext, int dZahl) {
		switch (rZiffer) {
		case 'I':
			if (rZifferNext != 'I') {
				dZahl--;
				break;
			} else {
				dZahl++;
				break;
			}
		case 'V':
			dZahl += 5;
			break;
		case 'X':
			if (rZifferNext != 'V' && rZifferNext != 'I') {
				dZahl -= 10;
				break;
			} else {
				dZahl += 10;
				break;
			}
		case 'L':
			dZahl += 50;
			break;
		case 'C':
			if (rZifferNext != 'L' &&rZifferNext != 'X' && rZifferNext != 'V' && rZifferNext != 'I') {
				dZahl -= 100;
				break;
			} else {
				dZahl += 100;
				break;
			}
		case 'D':
			dZahl += 500;
			break;
		case 'M':
			dZahl += 1000;
			break;
		default:
			break;
		}
		return dZahl;
	}
}
