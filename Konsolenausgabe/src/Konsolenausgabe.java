
public class Konsolenausgabe {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//AB-Konsolenausgabe 1
		
		// Aufgabe 1:		
		System.out.print("\"Hallo, wie gehts?\" ");
		System.out.println("- Danke, gut!");
		System.out.println("\"Hallo, wie gehts?\""+" "+"- Danke, gut!");
		System.out.println("// print gibt einen String aus, println gibt einen String aus UND erzeugt einen Zeilenumbruch!");
		
		System.out.println("");
		
		// Aufgabe 2
		String x = "*************";
		System.out.printf("%7.1s\n", x);
		System.out.printf("%8.3s\n", x);
		System.out.printf("%9.5s\n", x);
		System.out.printf("%10.7s\n", x);
		System.out.printf("%11.9s\n", x);
		System.out.printf("%12.11s\n", x);
		System.out.printf("%s\n", x);
		System.out.printf("%8.3s\n", x);
		System.out.printf("%8.3s\n", x);
		System.out.printf("%8.3s\n", x);
		
		System.out.println("");
		
		//Aufgabe 3
		double a = 22.4234234;
		double b = 111.2222;
		double c = 4.0;
		double d = 1000000.551;
		double e = 97.34;
		
		System.out.printf("%.2f\n", a);
		System.out.printf("%.2f\n", b);
		System.out.printf("%.2f\n", c);
		System.out.printf("%.2f\n", d);		
		System.out.printf("%.2f\n", e);
		
		
		System.out.println("");
		System.out.println("");
		
		//AB-Konsolenausgabe 2

		// Aufgabe 1:
		
		String y = "*";
		
		System.out.printf("%7s"+ "*\n" , y);
		System.out.printf("%4s", y);
		System.out.printf("%7s\n", y);
		System.out.printf("%4s", y);
		System.out.printf("%7s\n", y);
		System.out.printf("%7s"+ "*\n" , y);
		
		System.out.println("");
		System.out.println("");
		
		// Aufgabe 2:
		
		System.out.printf("%-5s", "0!");
		System.out.printf("%-19s", "=");
		System.out.printf("%s", "=");
		System.out.printf("%4s\n", "1");
		
		System.out.printf("%-5s", "1!");
		System.out.printf("%-19s", "=");
		System.out.printf("%s", "=");
		System.out.printf("%4s\n", "1");
		
		System.out.printf("%-5s", "2!");
		System.out.printf("%-19s", "=");
		System.out.printf("%s", "=");
		System.out.printf("%4s\n", "2");
		
		System.out.printf("%-5s", "3!");
		System.out.printf("%-19s", "=");
		System.out.printf("%s", "=");
		System.out.printf("%4s\n", "6");
		
		System.out.printf("%-5s", "4!");
		System.out.printf("%-19s", "=");
		System.out.printf("%s", "=");
		System.out.printf("%4s\n", "24");
		
		System.out.printf("%-5s", "5!");
		System.out.printf("%-19s", "=");
		System.out.printf("%s", "=");
		System.out.printf("%4s\n", "120");
		
		System.out.println("");
		
		//Aufgabe 3:
		
		String F = "Fahrenheit";
		String C = "Celsius";
		
		String ff = "-20";
		String gf = "-10";
		String hf = "+0";
		String jf = "+20";
		String kf = "+30";
		
		double fc = -28.8889;
		double gc = -23.3333;
		double hc = -17.7778;
		double jc = -6.6667;
		double kc = -1.1111;
				
		
		System.out.printf("%-12s", F);
		System.out.print("|");
		System.out.printf("%10s\n", C);
		System.out.println("------------------------");
		
		System.out.printf("%-12s", ff);
		System.out.print("|");
		System.out.printf("%10.2f\n", fc);
	
		System.out.printf("%-12s", gf);
		System.out.print("|");
		System.out.printf("%10.2f\n", gc);
		
		System.out.printf("%-12s", hf);
		System.out.print("|");
		System.out.printf("%10.2f\n", hc);
		
		System.out.printf("%-12s", jf);
		System.out.print("|");
		System.out.printf("%10.2f\n", jc);
		
		System.out.printf("%-12s", kf);
		System.out.print("|");
		System.out.printf("%10.2f\n", kc);
	}

}

