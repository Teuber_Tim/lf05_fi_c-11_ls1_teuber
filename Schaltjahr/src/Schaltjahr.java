import java.util.Scanner;

public class Schaltjahr {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Willkommen!");
		System.out.println("Dieses Porgramm sagt ihnen ob das eingegebene Jahr ein Schaltjahr ist.");
		System.out.println("");
		int jahreszahl = eingabeJahr(tastatur, "Bitte geben sie eine Jahreszahl ein: ");
		schaltjahr(jahreszahl);

		tastatur.close();

	}

	public static int eingabeJahr(Scanner tastatur, String text) {
		System.out.printf(text);
		int jahr = tastatur.nextInt();
		return jahr;
	}

	public static void schaltjahr(int jahr) {
		if (jahr < 1583) {
			System.out.println("Vor dem Jahr 1583 wurden Schaltjahre noch nicht unterschieden!");
		}
		if ((jahr % 4) > 0) {
			System.out.printf("Das Jahr %d ist kein Schaltjahr!", jahr);

		} else if ((jahr % 100) > 0) {

			if ((jahr % 400) == 0) {
				System.out.printf("Das Jahr %d ist ein Schaltjahr!", jahr);
			} else {
				System.out.printf("Das Jahr %d ist kein Schaltjahr!", jahr);
			}
		} else {
			System.out.printf("Das Jahr %d ist ein Schaltjahr!", jahr);
		}
	}
}
