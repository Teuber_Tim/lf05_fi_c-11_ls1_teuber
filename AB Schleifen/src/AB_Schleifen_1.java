// AB_Schleifen_1 Aufgabe 5: Einmaleins
public class AB_Schleifen_1 {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int zahl;
		System.out.printf("Willkommen!\nDies ist das kleine Einmaleins.\n\n");
		for (int i = 1; i <= 10; i++) {
			for (int k = 1; k <= 10; k++) {
				zahl = i * k;
				if (k == 10) {
					if (zahl < 100) {
						System.out.println(" " + zahl + " ");
					} else {
						System.out.println(zahl + " ");
					}

				} else {
					if (zahl < 100 && zahl >= 10) {
						System.out.print(" " + zahl + " ");
					} else if (zahl < 10) {
						System.out.print("  " + zahl + " ");
					} else {
						System.out.print(zahl+" ");
					}

				}
			}
		}
	}

}
