//
// AB_Schleifen_2 Aufgabe 8: Matrix
import java.util.Scanner;

public class AB_Schleifen_2_while {
	static Scanner tastatur = new Scanner(System.in);

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int zahl=0, ziffer,i=1;
 
			System.out.printf("Willkommen!\n\nDiese Programm erstellt eine Multiplikationsmatrix.\n\n");
			ziffer=eingabeInt("Bitte geben Sie eine Ziffer zwischen 2 und 9 ein: ");

			if (ziffer<2 || ziffer > 9) {
				System.out.println("Fehlerhafte Eingabe!!");
			}
			else { 
				System.out.println("Multiplikationsmatrix");
			do {
				
				if (zahlEnthalten(zahl,ziffer) == true && zahl !=0) {
					if (i==10) {
						System.out.println("  * ");
						i=0;
					}else {
						System.out.print("  * ");
					}
				}
				else if (quersumme(ziffer,zahl)==true && zahl !=0) {
					if (i==10) {
						System.out.println("  * ");
						i=0;;
					}else {
						System.out.print("  * ");
					}
				}
				else if (teilbarkeit(ziffer,zahl)==true && zahl !=0) {
					if (i==10) {
						System.out.println("  * ");
						i=0;;
					}else {
						System.out.print("  * ");
					}
				}
				else if (i==10) {
					System.out.println(" "+zahl);
					i=0;		
					}

				else {
					if (zahl >= 10) {
						System.out.print(" " + zahl + " ");
					} else if (zahl < 10) {
						System.out.print("  " + zahl + " ");
//					} else {
//						System.out.print(zahl+" ");
				}

				}
				zahl++;
				i++;
			}while (zahl!=100);
			
			}}

	public static int eingabeInt(String text) {
		System.out.printf(text);
		int zahl = tastatur.nextInt();
		return zahl;
	}

	public static boolean zahlEnthalten(int wert, int ziffer) {
		while (wert > 0) {
			if (wert % 10 == ziffer) {
				return true;
			}
			wert /= 10;
		}
		return false;
	}

    public static boolean quersumme(int ziffer, int zahl) {  
        int quer, rest;
        quer = 0;
        while (zahl > 0) {
            rest = zahl % 10;
            quer = quer + rest;
            zahl = zahl / 10;
            
        }
        if (ziffer== quer) {
        	return true;
        }
        return false;
    }
    public static boolean teilbarkeit(int ziffer, int zahl) {
    	if (zahl%ziffer==0) {
    		return true;
    	}
    		return false;
    }
}
