import java.util.Scanner;

public class MittelwertMitArrays {
    public static void main(String[] args) {
        // TODO Auto-generated method stub
        double summe=0,mittelwert;
        int anzahlWerte;
        Scanner myScanner = new Scanner(System.in);
        programmhinweis("Diese Programm berechnet den Mittelwert mehrere Zahlen.");
        anzahlWerte = eingabeAnzahl(myScanner, "Geben Sie die Anzahl der einzugebenden Werte an: ");
        double[] werte = new double[anzahlWerte];
        for(int i=0; i < anzahlWerte; i++) {
            werte[i] = eingabeDouble(myScanner, "Neuen Wert eingeben: ");
        }
        for(int i=0; i < anzahlWerte; i++) {
        	System.out.println(werte[i]);
        }

        for (int i=0; i<werte.length; i++){
            summe+=werte[i];
        }
//		zahl1 = eingabe(myScanner,"Bitte eben sie die erste Zahl ein: ");
//		zahl2 = eingabe(myScanner,"Bitte geben sie die zweite Zahl ein: ");
        mittelwert = mittelwertBerechnung(summe,(double)anzahlWerte);
        ausgabe ("Der Mittelwert ist %.2f!",mittelwert);
        myScanner.close();

    }
    public static void programmhinweis(String text) {
        System.out.println(text);
    }
    public static int eingabeAnzahl(Scanner myScanner,String text) {
        System.out.println(text);
        int eingabe = myScanner.nextInt();
        return eingabe;
    }
    public static double eingabeDouble(Scanner myScanner, String text) {
        System.out.println(text);
        double eingabe = myScanner.nextDouble();
        return eingabe;
    }
    public static double mittelwertBerechnung(double x, double y) {
        double ergebnis = (x)/y;
        return ergebnis;
    }
    public static void ausgabe (String text, double z) {
        System.out.printf(text,z);
    }
}
