import java.util.Scanner;
public class Aufgabe2 {
//public class PCHaendler {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		String artikel; int anzahl; double nettogesamtpreis, bruttogesamtpreis, mwst, preis;
		artikel=liesString( myScanner, "Was m�chten Sie bestellen? ");
		anzahl=liesInt ( myScanner, "Geben Sie die Anzahl ein: " );
		preis=liesDouble(myScanner, "Geben Sie den Nettopreis ein: ");
		mwst=liesDouble(myScanner, "Geben Sie Mehrwertsteuersatz in Prozent ein: ");
		
		nettogesamtpreis = berechneGesamtnettopreis(anzahl, preis);
		bruttogesamtpreis =berechneGesamtbruttopreis(nettogesamtpreis, mwst);
		rechungausgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);
	
		
	}
	public static String liesString(Scanner myScanner,String text) {
		// Benutzereingaben lesen
		System.out.println(text);
		String artikel = myScanner.next();
		return artikel;
	}
	public static int liesInt(Scanner myScanner, String text) {
		System.out.println(text);
		int anzahl = myScanner.nextInt();
		return anzahl;
	}
	public static double liesDouble(Scanner myScanner, String text) {
		System.out.println(text);
		double preis = myScanner.nextDouble();
		return preis;
	}
	
	//	System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
	//	double mwst = myScanner.nextDouble();

		// Verarbeiten
	public static double berechneGesamtnettopreis(int anzahl, double preis) {
		double nettogesamtpreis = anzahl * preis;
		return nettogesamtpreis;
	}
	public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst) {
		
		double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);
		return bruttogesamtpreis;
	}

		// Ausgeben
	public static void rechungausgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mwst) {
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	}
		
}



 


 
