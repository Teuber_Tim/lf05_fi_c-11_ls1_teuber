import java.util.Scanner;

public class MittelwertMitSchleifen {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double summe=0,mittelwert;
		int anzahlWerte;
		Scanner myScanner = new Scanner(System.in);
		programmhinweis("Diese Programm berechnet den Mittelwert mehrere Zahlen.");
		anzahlWerte = eingabeAnzahl(myScanner, "Geben Sie die Anzahl der einzugebenden Werte an: ");
		
		for(int i=0; i < anzahlWerte; i++) {
			summe += eingabeDouble(myScanner, "Neuen Wert eingeben: ");
		}
//		zahl1 = eingabe(myScanner,"Bitte eben sie die erste Zahl ein: ");
//		zahl2 = eingabe(myScanner,"Bitte geben sie die zweite Zahl ein: ");
		mittelwert = mittelwertBerechnung(summe,(double)anzahlWerte);
		ausgabe ("Der Mittelwert ist %.2f!",mittelwert);
		myScanner.close();

	}
	public static void programmhinweis(String text) {
		System.out.println(text);
	}
	public static int eingabeAnzahl(Scanner myScanner,String text) {
		System.out.println(text);
		int eingabe = myScanner.nextInt();
		return eingabe;
	}
	public static double eingabeDouble(Scanner myScanner, String text) {
		System.out.println(text);
		double eingabe = myScanner.nextDouble();
		return eingabe;
	}
	public static double mittelwertBerechnung(double x, double y) {
		double ergebnis = (x)/y;
		return ergebnis;
	}
	public static void ausgabe (String text, double z) {
		System.out.printf(text,z);
	}
}